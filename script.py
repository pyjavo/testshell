import os
import logging
from time import sleep, time
from json import JSONDecodeError
from math import ceil

import requests
from google.cloud import storage
from google.cloud import bigquery
import google.cloud.logging
from dotenv import load_dotenv
import sentry_sdk


def make_query(client, tablename, LIMIT, offset, get_total=False):
    '''Makes query to BigQuery

    Args:
    get_total: True if we only want to know total rows of a table.
    If get_total is False, then make the query using LIMIT and OFFSET
    '''

    QUERY = f"SELECT * FROM {GCP_PROJECT_ID}.shared.{tablename}"

    if not get_total:
        QUERY += f" LIMIT {LIMIT} OFFSET {offset}"

    query_job = client.query(QUERY)
    results = query_job.result()  # Wait for query to complete.
    rows_len = results.total_rows

    return query_job, rows_len


def find_offset_values(rows_len):
    '''Find out the OFFSET values for que query.

    Args:
    rows_len: total number of rows from the Bigquery Table.

    Example:
    - If rows_len is 19463, then offset_values are: [0, 4900, 9800, 14700]
    '''
    list_len = ceil(rows_len/LIMIT)
    temp_limit = 0
    offset_values = []

    for item in range(list_len):
        offset_values.append(temp_limit)
        temp_limit += 4900

    return offset_values


def insert_data_gcp(rows_to_insert, tablename):
    '''
    Insert data into GCP

    Docs: https://cloud.google.com/bigquery/docs/samples/bigquery-table-insert-rows?hl=es-419
    '''

    logging.info(
        f'Inserting data: {rows_to_insert}'
    )

    errors = client.insert_rows_json(
        f'{GCP_PROJECT_ID}.shared.Bankruptcy',
        rows_to_insert
    )  # Make an API request.

    if errors == []:
        logging.info(f'New rows have been added for : {tablename}')
        return True
    else:
        logging.error(f"Encountered errors while inserting rows in {tablename}: {errors} ")
        logging.info(f'These were the rows that were not inserted {rows_to_insert}')
    return False


def get_registers(query_params, query_job, searched_names, sleep_step, retry_counter, tablename, req_counter):
    '''Request information to the API, looping the registers from the table.

    Args:
    retry_counter: counter of requests
    '''

    for register in query_job:
        sleep(sleep_step)
        rows_to_insert = []

        if not register['Grantee1NameFull']:
            continue
        grantee_name = register['Grantee1NameFull']

        if register['Grantor1NameFull']:
            grantor_name = register['Grantor1NameFull']
        else:
            grantor_name = ''

        if grantee_name.lower() not in searched_names:
            searched_names.add(grantee_name.lower())
            query_params["case_name"] = grantee_name

            response = requests.get(search_url, headers=my_headers, params=query_params)

            req_counter += 1
            if req_counter and req_counter % 100 == 0:
                # Print every 100 records
                logging.info(f'- Request counter #{req_counter}')

            try:
                if response.status_code == 200 and response.json()['results']:
                    results_list = response.json()['results']
                    for result in results_list:
                        if not result['party']:
                            continue
                        comparison = grantee_name.lower() in (string.lower() for string in result['party'])
                        if comparison:
                            logging.info(f'#{req_counter} - Results for Grantee: {grantee_name}')
                            my_dict = {}
                            my_dict['party'] = ', '.join(result['party'])  # concatenated
                            my_dict['Grantor1NameFull'] = grantor_name
                            my_dict['bbl'] = register['bbl']
                            my_dict['url'] = PATH + result['absolute_url']
                            my_dict['docket_id'] = str(result['docket_id'])
                            my_dict['caseName'] = result['caseName']
                            my_dict['court'] = result['court']
                            my_dict['court_id'] = result['court_id']
                            my_dict['Grantee1NameFull'] = grantee_name
                            my_dict['distress_signal'] = 'Bankruptcy or Litigation'
                            my_dict['Mortgage1Amount'] = register['Mortgage1Amount']
                            my_dict['date_created'] = time()

                            rows_to_insert.append(my_dict)
                            insert_data_status = insert_data_gcp(rows_to_insert, tablename)

                            if not insert_data_status:
                                logging.error(
                                    f'__ Error happened for #{req_counter} - data was: {my_dict}'
                                )
                elif response.status_code != 200:
                    logging.warning(
                        f'We got a status code {response.status_code} from the server. Waiting 5 minutes.'
                    )
                    sleep(300)

            except JSONDecodeError as error:
                logging.error(
                    f'There was a JSONDecodeError. Status code: {response.status_code}'
                )
                logging.error('=== JSONDecodeError message === ' + error)
            except ConnectionResetError as e:
                logging.error('=== ConnectionResetError === ' + e)
                logging.warning('Going to sleep for 3 minutes.....')
                sleep(180)
                sleep_step += 0.55
                logging.warning(
                    f'Sleep time for the request loop is now: {sleep_step} seconds.'
                )

                retry_counter += 1

                if retry_counter > 4:
                    logging.warning(
                        f'Breaking the loop because retry counter is {retry_counter} times'
                    )
                    break

    return req_counter, searched_names, sleep_step, retry_counter


def run(tablename, offset_values, searched_names, sleep_step, retry_counter):
    '''Run the main functions
    '''
    req_counter = 0

    for offset in offset_values:
        logging.info(f'Now making query for OFFSET: {offset}')
        query_job, _ = make_query(client, tablename, LIMIT, offset, get_total=False)

        req_counter, searched_names, sleep_step, retry_counter = get_registers(
            query_params,
            query_job,
            searched_names,
            sleep_step,
            retry_counter,
            tablename,
            req_counter
        )

        logging.info(f'======= Finished OFFSET: {offset} for {tablename} =======')
        logging.info(f'= Request counter used: {req_counter}')
        logging.info(f'= Lenght of searched names: {len(searched_names)}')
        logging.info(f'= Sleep step used: {sleep_step}')
        logging.info(f'= Retry counter used: {retry_counter}')

    return sleep_step


if __name__ == '__main__':
    # Configure logging

    logging_client = google.cloud.logging.Client()
    logging_client.setup_logging()

    # Load env variables
    filename = 'bankruptcy/.env'

    storage_client = storage.Client()
    bucket = storage_client.get_bucket('skw-data-lake')
    blob = bucket.blob(filename)
    blob.download_to_filename('.env')

    load_dotenv()

    GCP_PROJECT_ID = os.getenv('GCP_PROJECT_ID')
    GCP_TOKEN = os.getenv('GCP_TOKEN')
    PATH = os.getenv('API_PATH')
    SENTRY_DSN = os.getenv('SENTRY_DSN')

    sentry_sdk.init(
        dsn=SENTRY_DSN,

        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production.
        traces_sample_rate=1.0
    )

    logging.info(f'GCP_PROJECT_ID set to: {GCP_PROJECT_ID}')
    logging.info(f'GCP_TOKEN set to: {GCP_TOKEN}')
    logging.info(f'API_PATH set to: {PATH}')

    # Bigquery Query configurtion

    client = bigquery.Client(project=GCP_PROJECT_ID)

    # API Configuration

    search_url = PATH + "/api/rest/v3/search/"
    bankruptcy_courts = "bap1 bap2 bap6 bap8 bap9 bap10 bapme bapma cacb caeb canb casb dcb nyeb nynb nysb nywb rib"
    type_search = "d"
    my_headers = {'Authorization': f'Token {GCP_TOKEN}'}
    query_params = {
        "court": bankruptcy_courts,
        "type": type_search,
    }

    searched_names = set()
    sleep_step = 5
    retry_counter = 0
    LIMIT = 4900

    # To simulate the create or replace effect in BigQuery

    QUERY = f"TRUNCATE TABLE {GCP_PROJECT_ID}.shared.Bankruptcy"
    query_job1 = client.query(QUERY)

    # LA process

    tablename = 'LA_PTS'
    _, len_total_rows = make_query(client, tablename, 4900, 0, get_total=True)
    offset_values = find_offset_values(len_total_rows)
    # offset_values = [6400, 9800, 14700]  # TODO: Remove hardcoded
    logging.info(f'======= OFFSET values: {offset_values}')
    sleep_step = run(tablename, offset_values, searched_names, sleep_step, retry_counter)
    logging.info(f'======= DB {tablename} finished with sleep of {sleep_step}secs =======')

    # NY process
    tablename = 'NY_PTS'
    _, len_total_rows = make_query(client, tablename, 4900, 0, get_total=True)
    offset_values = find_offset_values(len_total_rows)
    logging.info(f'======= OFFSET values: {offset_values}')
    sleep_step = run(tablename, offset_values, searched_names, sleep_step, retry_counter)
    logging.info(f'======= DB {tablename} finished with sleep of {sleep_step}secs =======')
