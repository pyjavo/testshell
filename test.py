import os
import sys
import logging

from google.cloud import storage
from google.cloud import bigquery
import google.cloud.logging
from dotenv import load_dotenv
import sentry_sdk


def another_test():
    # Bigquery Query configurtion

    project_id = GCP_PROJECT_ID
    client = bigquery.Client(project=project_id)

    # ======= API Config

    search_url = PATH + "/api/rest/v3/search/"
    bankruptcy_courts = "bap1 bap2 bap6 bap8 bap9 bap10 bapme bapma cacb caeb canb casb dcb nyeb nynb nysb nywb rib"
    type_search = "d"
    my_headers = {'Authorization' : f'Token {GCP_TOKEN}'}
    query_params = {
        "court": bankruptcy_courts,
        "type": type_search,
    }


    searched_names = set()
    sleep_step = 0.55
    retry_counter = 0
    LIMIT = 4900


def just_testing():
    # Test for custom DATABASE

    offset_values = [6400, 11301]

    tablename = 'DATABASE'
    LIMIT = 4900
    GCP_PROJECT_ID="mi-proyecto-5-678901"

    offset = 6400

    # ¿Debería llegar hasta rows_len que es 15472 y no 11301+4900 = 16201? --> Doesn't matter
    QUERY = f"SELECT * FROM {GCP_PROJECT_ID}.shared.{tablename}"
    QUERY += f" LIMIT {LIMIT} OFFSET {offset}"

    print(QUERY)

    '''
    rows_len = 15472
    get_total=False

    # for rows_len= 15472
    offset_values1 = [0, 4900, 9800, 14700]

    for offset in offset_values:
        print(f"Now making query for OFFSET: {offset}")
        QUERY = f"SELECT * FROM {GCP_PROJECT_ID}.shared.{tablename}"

        if not get_total:
            QUERY += f" LIMIT {LIMIT} OFFSET {offset}"

        print(QUERY, end='\n\n')
    '''


def main():
    # Configure logging

    logging_client = google.cloud.logging.Client()
    logging_client.setup_logging()

    # Load env variables
    filename = 'bankruptcy/.env'

    storage_client = storage.Client()
    bucket = storage_client.get_bucket('skw-data-lake')
    blob = bucket.blob(filename)
    blob.download_to_filename('.env')

    load_dotenv()

    GCP_PROJECT_ID = os.getenv('GCP_PROJECT_ID')
    GCP_TOKEN = os.getenv('GCP_TOKEN')
    PATH = os.getenv('API_PATH')
    SENTRY_DSN = os.getenv('SENTRY_DSN')

    sentry_sdk.init(dsn=SENTRY_DSN,traces_sample_rate=1.0)

    logging.info(f'======== GCP_TOKEN set to: {GCP_TOKEN}')
    # logging.info(f'======== GCP_PROJECT_ID set to: {GCP_PROJECT_ID}')
    # logging.info(f'======== API_PATH set to: {PATH}')

    logging.info(f'======== Current directory is {os.getcwd()}')
    logging.info(f'======== Python executable is {sys.executable}')

    # text = '###### Finishing message ####### '
    # logging.warning(text)


if __name__ == '__main__':
    main()

    # no lo apaga
    # logging.info('___________ Shutting down the system __________')

    # os.system("shutdown /s /t 1")
