google-api-python-client
gcloud==0.18.3
google-cloud-bigquery  # https://github.com/googleapis/python-bigquery
python-dotenv==0.20.0
google-cloud-storage
google-cloud-logging
sentry-sdk==1.9.3
selenium==4.3.0
anticaptchaofficial
