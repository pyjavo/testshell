#!/usr/bin/env bash

sudo apt update
sudo apt install software-properties-common

sudo add-apt-repository ppa:deadsnakes/ppa

sudo apt install python3.9 -y

sudo apt install python3-pip -y

sudo apt-get install -yq git supervisor

python3.9 -m pip install pip

# Account to own server process
useradd -m -d /home/pythonapp pythonapp

[[ -d opt/app ]] || mkdir opt/app

# Set ownership to newly created account
chown -R pythonapp:pythonapp /opt/app

cd opt/app/

[[ -d testshell ]] || git clone https://gitlab.com/pyjavo/testshell.git

cd testshell/
git pull origin main

python3.9 -m pip install -r requirements.txt

# Put supervisor configuration in proper place
cp /opt/app/testshell/python-app.conf /etc/supervisor/conf.d/python-app.conf


# Start service via supervisorctl
supervisorctl reread
supervisorctl update

# sudo python3.9 script.py
# sudo python3.9 test.py

#shutdown -h now